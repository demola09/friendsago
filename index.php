<?php
session_start();
$signup_error= $_SESSION["signup_error"];
$signup_success=$_SESSION["message_success"];
//echo $signup_error;
?>


<!DOCTYPE html>

<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			FriendsAgo | Login Page
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<!--end::Web font -->
        <!--begin::Base Styles -->
		<link href="assets/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="images/logo.png" />
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login" style="background-image: url(images/bg-3.jpg);">
				<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
					<div class="m-stack m-stack--hor m-stack--desktop">
						<div class="m-stack__item m-stack__item--fluid">
							<div class="m-login__wrapper">
								<div class="m-login__logo">
									<a href="#">
								<img src="images/image1.png">
									</a>
								</div>
								<div class="m-login__signin">
									<div class="m-login__head">
										<h3 class="m-login__title">
											FriendsAgo 
										</h3>
									</div>
									<?php echo $_SESSION["login_error"];


										unset($_SESSION["login_error"]);
									?>
										<?php
										if($signup_success){
										echo "<div class='alert alert-success text-center'> $signup_success</div>";
										unset($_SESSION["signup_success"]);
										}

										 ?>

					
									<form class="m-login__form m-form" method="post" action="login.php">
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off" required>
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" required>
										</div>
										<div class="row m-login__form-sub">
											<div class="col m--align-left">
												<label class="m-checkbox m-checkbox--focus">
													<input type="checkbox" name="remember">
													Remember me
													<span></span>
												</label>
											</div>
											<div class="col m--align-right">
												<a href="javascript:;" id="m_login_forget_password" class="m-link">
													Forget Password ?
												</a>
											</div>
										</div>
										<div class="m-login__form-action">
											<button  class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air"><!--id="m_login_signin_submit"-->
												Sign In
											</button>
										</div>
									</form>
								</div>
								<div class="m-login__signup">
									<div class="m-login__head">
										<h3 class="m-login__title">
											Sign Up
										</h3>
										<div class="m-login__desc">
											Enter your details to create your account:
										</div>
									</div>
									
										
									<?php echo $_SESSION["message_success"]; 
										unset($_SESSION["message_success"]);
										?>
									<form class="m-login__form m-form" method="post" action="signup.php">
										<?php
										if($signup_error){
										echo "<div class='alert alert-danger text-center'> $signup_error</div>";
										unset($_SESSION["signup_error"]);
										}

										 ?>
										
										
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="text" placeholder="Fullname" name="fullname">
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off">
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input" id="password" type="password" placeholder="Password" name="password">
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input" id="confirm_password" type="password" placeholder="Confirm Password" name="confirm_password">
											<span id='message'></span>
										</div>
										<div class="row form-group m-form__group m-login__form-sub">
											<div class="col m--align-left">
												<label class="m-checkbox m-checkbox--focus">
													<input type="checkbox" name="agree">
													I Agree to the
													<a href="#" class="m-link m-link--focus">
														terms and conditions
													</a>
													.
													<span></span>
												</label>
												<span class="m-form__help"></span>
											</div>
										</div>
										<div class="m-login__form-action">
											<button  class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air"> <!--id="m_login_signup_submit" -->  
												Sign Up
											</button>
											<button id="m_login_signup_cancel" class="btn btn-outline-focus  m-btn m-btn--pill m-btn--custom">
												Cancel
											</button>
										</div>
									</form>
								</div>
								<div class="m-login__forget-password">
									<div class="m-login__head">
										<h3 class="m-login__title">
											Forgotten Password ?
										</h3>
										<div class="m-login__desc">
											Enter your email to reset your password:
										</div>
									</div>
									<form class="m-login__form m-form" action="">
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
										</div>
										<div class="m-login__form-action">
											<button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
												Request
											</button>
											<button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
												Cancel
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="m-stack__item m-stack__item--center">
							<div class="m-login__account">
								<span class="m-login__account-msg">
									Don't have an account yet ?
								</span>
								&nbsp;&nbsp;
								<a href="javascript:;" id="m_login_signup" class="m-link m-link--focus m-login__account-link">
									Sign Up
								</a>
							</div>
						</div>
					</div>
				</div>

				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content" style="background-image: url(images/bg-4.jpg);">
					<div class="m-grid__item m-grid__item--middle">
						<h3 class="m-login__welcome">
							FriendsAgo  
						</h3>
						<p class="m-login__msg">
							FriendsAgo  
							<br>
							By signing up .........
						</p >

						<p class="m-login__msg" >Contact our support team-> &nbsp Email: &nbsplabkins@yahoo.com, Phone number:&nbsp 08067862657</p>

					</div>
				</div>
			</div>
		</div>
		<!-- end:: Page -->
    	<!--begin::Base Scripts -->
    	
		<script src="assets/vendors.bundle.js" type="text/javascript"></script>
		<script src="assets/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Base Scripts -->   
        <!--begin::Page Snippets -->
		<script src="assets/login.js" type="text/javascript"></script>
		<script>
    		    
				$('#password, #confirm_password').on('keyup', function () {
					var password = $('#password').val();
					if(!password){
 						$('#message').html('Enter a password').css('color', 'red');
					}
					else if(password.length < 6){
						 $('#message').html('Password must be at least 6 characters').css('color', 'red');
					}
				  else if ($('#password').val() == $('#confirm_password').val()) {
				    $('#message').html('Matching').css('color', 'green');
				  } else 
				    $('#message').html('Not Matching').css('color', 'red');
				});



				$(document).ready(function(){
					var isSignUp = '<?php echo $signup_error ?>'
    		    //alert(isSignUp);
    		    if(isSignUp){
    		    	$('#m_login_signup').click();		    }
				})


    	</script>
		<!--end::Page Snippets -->
	</body>
	<!-- end::Body -->
</html>
