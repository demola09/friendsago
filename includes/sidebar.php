<?php
 $loc = this.location; 
 $_status= $_SESSION['user_status'];
?>

<div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="#" class="simple-text">
                     <i class="material-icons">person</i><?php echo $_SESSION['user_status']; ?>
                </a>
            </div>
            <div class="sidebar-wrapper">
                 <ul class="nav">
                    <li class="index <?php if ($page == "index") { echo " active"; } ?>"><a href="dashboard.php">Home</a></li>
                    <li class="dashboard <?php if ($page == "dashboard") { echo " active"; } ?> "> 
                        <a href="dashboard.php">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="<?php if(this.href == $loc){echo 'class="active"';} else {echo '';} ?>">
                        <a href="./user.php">
                            <i class="material-icons">person</i>
                            <p>User Profile</p>
                        </a>
                    </li>
                    <li class="<?php if(this.href == $loc){echo 'class="active"';} else {echo '';} ?> ">
                        <a href="#">
                            <i class="material-icons">person</i>
                            <p>Friend Requests</p>
                        </a>
                    </li>
                    
                    <li class="<?php if(this.href == $loc){echo 'class="active"';} else {echo '';} ?>">
                        <a href="#">
                            <i class="material-icons text-gray">content_paste</i>
                            <p>Messages</p>
                        </a>
                    </li>

                    <li class="">
                        <a href="logout.php">
                            <i class="material-icons text-gray">person</i>
                            <p>Logout</p>
                        </a>
                    </li>
                    
                    <li class="<?php if(this.href == $loc){echo 'class="active"';} else {echo '';} ?> " style="<?php if ($_status !== "Admin" ) { echo "display:none"; } ?>">
                        <a href="#">
                            <i class="material-icons">content_paste</i>
                            <p>Admin</p>
                        </a>
                    </li>
					 
					 
					 
					 <!--<li class="<?php if(this.href == $loc){echo 'class="active"';} else {echo '';} ?>" style="<?php if ($_status != "admin") { echo "display:none"; } ?>">
                        <a href="download.php">
                            <i class="material-icons text-gray">content_paste</i>
                            <p>Download Report</p>
                        </a>
                    </li>

                    <li class="<?php if(this.href == $loc){echo 'class="active"';} else {echo '';} ?>" style="<?php if ($_status != "admin") { echo "display:none"; } ?>">
                        <a href="prescription.php">
                            <i class="material-icons text-gray">content_paste</i>
                            <p>Prescriptions</p>
                        </a>
                    </li>

                    <li class="<?php if(this.href == $loc){echo 'class="active"';} else {echo '';} ?>">
                        <a href="user_patients.php">
                            <i class="material-icons text-gray">content_paste</i>
                            <p>View Records</p>
                        </a>
                    </li>-->


                </ul>
            </div>
</div>




<script>
    
$parent.find('button[name=download]').click(function () {
    window.location.href = 'download.php';
});


</script>